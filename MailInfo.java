package dk.enron_emails;

import java.util.Date;
import java.util.List;

/**
 * @author Norus
 * 
 * Pojo to hold mail data
 *
 */

public class MailInfo implements IMailInfo,Comparable<MailInfo> {
	

	private List<String> to;
	private String from;
	private String subject;
	private Date time;

	public List<String> getTo() {
		return to;
	}

	public String getFrom() {
		return from;
	}

	public String getSubject() {
		return subject;
	}

	public Date getTime() {
		return time;
	}

	public void setTo(List<String> to) {
		this.to = to;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public void setTime(Date time) {
		this.time = time;
	}
	
	String getToFromEmailAdresses() {
		return getTo() +  getFrom();
	}
	
	public String pretty() {
		String result = getTo() + "\n";
		result += getFrom() + "\n";
		result += getSubject() + "\n";
		result += getTime() + "\n";
		result += "\n";
		return result;

	}
	
	  public int compareTo(MailInfo o) {
	    if (getTime() == null || o.getTime() == null)
	      return 0;
	    return getTime().compareTo(o.getTime());
	  }


}
