package dk.enron_emails;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/*
 * Class to search through Enron email's stored on disk. This class was developed as an assignment for a large
 * public institution.
 * The Enron dataset can be downloaded here : https://www.cs.cmu.edu/~enron/
 */

class MailHandler implements IMailHandler {
	private ArrayList<MailInfo> mailList = new ArrayList<MailInfo>();
	private String path;
	private int maxMails;
	private final static String from = "From:";
	private final static String to = "To:";
	private final static String split = ",";
	private final static String subject = "Subject:";
	private final static String dateStr = "Date: ";
	private final static String malformed = "malformed date : ";
	private final static String dateFormat= "EEE, d MMM yyyy HH:mm:ss Z";
	
	public MailHandler(String path) {
		
		this.path = path;
	}
	
	
	private void walk(String rootDir) {

		try {
			
			try (Stream<Path> paths = Files.find(Paths.get(rootDir), 
												Integer.MAX_VALUE,
										(path, file) -> file.isRegularFile())
										.limit(maxMails)) {
				
				paths.forEach(f -> {
					try {
							List<String> lines = Files.readAllLines(f);
							MailInfo mail = new MailInfo();
							lines.forEach(line -> {
		
							if (line.startsWith(from)) {
								mail.setFrom(line);
							} else if (line.startsWith(to)) {		
								mail.setTo(Arrays.asList(line.split(split)));
							} else if (line.startsWith(subject)) {
								mail.setSubject(line);
							} else if (line.startsWith(dateStr)) {
								SimpleDateFormat sdf = new SimpleDateFormat(dateFormat , Locale.ENGLISH);
								Date date = null;
								try {
									date = sdf.parse(line.replace(dateStr, ""));
								} catch (ParseException e) {
									System.out.println(malformed + line);
								}
								mail.setTime(date);
							}
						});  				
						
						mailList.add(mail);

					} catch (IOException e) {
						e.printStackTrace();
					}					
			    
				});
				
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void doImport(Integer maxMails) {
		this.maxMails = maxMails;
		walk(path);
	}

	public List<IMailInfo> search(String emailAddress, Date maxTime) {
		List<MailInfo> matches = new ArrayList<MailInfo>();
				
		matches = mailList.stream()               
                .filter(mailInfo -> emailAddress.equals(mailInfo) && mailInfo.getTime().before(maxTime))    
                .collect(Collectors.toList());  
        	
		Collections.sort(matches, Collections.reverseOrder());
		
		return (List) matches;
	}

	public List<IMailInfo> sample(Integer nSamples) {		
		
		Collections.shuffle( mailList );		
		
		return (List)mailList.subList(0, nSamples);
	}
	

}

